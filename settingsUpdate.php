<?php 
$myfile = fopen("settings.txt", "r") or die("Unable to open file!");
$settings = fread($myfile,filesize("settings.txt"));
fclose($myfile);
$settings = json_decode($settings);

$dataToSave = array();
foreach ($_POST as $key => $value) {
  $dataToSave[$key] = array("value" => $value, "type" => $settings->{$key}->{'type'});
}

if ($_POST != NULL) {
  // echo "<pre>";
  // var_dump($_POST);
  // echo "</pre>";
  // echo json_encode($_POST);
  $myfile = fopen("settings.txt", "w") or die("Unable to open file!");
  fwrite($myfile, json_encode($dataToSave));
  fclose($myfile);
  header("Refresh:0");
}



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title>IBM WAI visualiser</title>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
  <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="js/jscolor.min.js"></script>
</head>
<body>
  <div class="container">

    <form action="" method="post">
      <?php 
      foreach ($settings as $key => $value) {
        echo "<div class=\"form-group\">";
        echo "  <label for=\"".$key."\">".$key.":</label>\n";
        if ($value->{'type'} == "color") {
          echo "  <input type=\"text\" id=\"".$key."\" name=\"".$key."\" value=\"".$value->{'value'}."\" class=\"jscolor form-control\">\n";
        } else {
          echo "  <input type=\"text\" id=\"".$key."\" name=\"".$key."\" value=\"".$value->{'value'}."\" class=\"form-control\">\n";
        }
        echo "<div>\n";
      }
      ?>
<!--     <input class="jscolor" value="ab2567">
-->    
<div class="form-group">
  <input type="submit" class="btn btn-success" />
</div>
</form>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>
<?php
// $jsonSrc = array(
//     'firstColor' => array(
//         'value' => "#696",
//         'type' => 'color',
//     ),
//     "firstBorder" => array(
//         'value' => "85",
//         'type' => 'text',
//     ),
//     "secondColor" => array(
//         'value' => "#ff3030",
//         'type' => 'color',
//     ),
//     "secondBorder" => array(
//         'value' => "91",
//         'type' => 'text',
//     ),
//     "thirdColor" => array(
//         'value' => "#ff9930",
//         'type' => 'color',
//     ),
//     "sheetname" => array(
//         'value' => "Prior Mo SES Country",
//         'type' => 'text',
//     ),


// ); 
// echo json_encode($jsonSrc); 
?>