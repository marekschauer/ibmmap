<?php
error_reporting(E_ALL);
set_time_limit(0);

date_default_timezone_set('Europe/London');

// Getting settings stored in JSON format in file
$myfile = fopen("settings.txt", "r") or die("Unable to open file!");
$settings = fread($myfile,filesize("settings.txt"));
fclose($myfile);
$settings = json_decode($settings);

// Getting available .xls sources of data to read
$availableSources = glob("source_files/*.xls");
usort($availableSources, create_function('$a,$b', 'return filemtime($a) - filemtime($b);'));
$availableSources = array_reverse($availableSources);
if (count($availableSources) == 0) {
	echo "I cannot find any source files on the server.";
	die();
} else {
	$inputFileName = $availableSources[0];
}

/** Include path **/
set_include_path(get_include_path() . PATH_SEPARATOR . 'PHPExcel/Classes/');

/** PHPExcel_IOFactory */
include 'PHPExcel/IOFactory.php';


$inputFileType = 'Excel5';
//	$inputFileType = 'Excel2007';
//	$inputFileType = 'Excel2003XML';
//	$inputFileType = 'OOCalc';
//	$inputFileType = 'Gnumeric';


if (isset($_POST['srcId']) && $_POST['srcId'] < count($availableSources) && file_exists($availableSources[$_POST["srcId"]])) {
	$inputFileName = $availableSources[$_POST["srcId"]];
}

$sheetname = $settings->{'sheetname'}->{'value'};
$firstBorder = $settings->{'firstBorder'}->{'value'};
$secondBorder = $settings->{'secondBorder'}->{'value'};

class MyReadFilter implements PHPExcel_Reader_IReadFilter
{
	private $_startRow = 0;

	private $_endRow = 0;

	private $_columns = array();

	public function __construct($startRow, $endRow, $columns) {
		$this->_startRow	= $startRow;
		$this->_endRow		= $endRow;
		$this->_columns		= $columns;
	}

	public function readCell($column, $row, $worksheetName = '') {
		if ($row >= $this->_startRow && $row <= $this->_endRow) {
			if (in_array($column,$this->_columns)) {
				return true;
			}
		}
		return false;
	}
}

$filterSubset = new MyReadFilter(2,1000,range('C','I'));



// echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory with a defined reader type of ',$inputFileType,'<br />';
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
// echo 'Loading Sheet "',$sheetname,'" only<br />';
$objReader->setLoadSheetsOnly($sheetname);
// echo 'Loading Sheet using configurable filter<br />';
$objReader->setReadFilter($filterSubset);
$objPHPExcel = $objReader->load($inputFileName);


// echo '<hr />';
// echo '<pre>';
$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
// var_dump($sheetData);
// echo '</pre>';

$shortCodes = array(
	"INDONESIA" => "id",
	"PAPUA NEW GUINEA" => "pg",
	"MEXICO" => "mx",
	"ESTONIA" => "ee",
	"ALGERIA" => "dz",
	"MOROCCO" => "ma",
	"MAURITANIA" => "mr",
	"SENEGAL" => "sn",
	"GAMBIA" => "gm",
	"GUINEA-BISSAU" => "gw",
	"GUINEA" => "gn",
	"SIERRA LEONE" => "sl",
	"LIBERIA" => "lr",
	"COTE D'IVOIRE" => "ci",
	"MALI" => "ml",
	"BURKINA FASO" => "bf",
	"NIGER" => "ne",
	"GHANA" => "gh",
	"TOGO" => "tg",
	"BENIN" => "bj",
	"NIGERIA" => "ng",
	"TUNISIA" => "tn",
	"LIBYA" => "ly",
	"EGYPT" => "eg",
	"CHAD" => "td",
	"SUDAN" => "sd",
	"CAMEROON" => "cm",
	"ERITREA" => "er",
	"DJIBOUTI" => "dj",
	"ETHIOPIA" => "et",
	"SOMALIA" => "so",
	"YEMEN" => "ye",
	"CENTRAL AFRICAN REPUBLIC" => "cf",
	"SAO TOME AND PRINCIPE" => "st",
	"EQUATORIAL GUINEA" => "gq",
	"GABON" => "ga",
	"CONGO" => "cd",
	"ANGOLA" => "ao",
	"RWANDA" => "rw",
	"BURUNDI" => "bi",
	"UGANDA" => "ug",
	"KENYA" => "ke",
	"TANZANIA" => "tz",
	"ZAMBIA" => "zm",
	"MALAWI" => "mw",
	"MOZAMBIQUE" => "mz",
	"ZIMBABWE" => "zw",
	"NAMIBIA" => "na",
	"BOTSWANA" => "bw",
	"SWAZILAND" => "sz",
	"LESOTHO" => "ls",
	"SOUTH AFRICA" => "za",
	"GREENLAND" => "gl",
	"AUSTRALIA" => "au",
	"NEW ZEALAND" => "nz",
	"NEW CALEDONIA" => "nc",
	"MALAYSIA" => "my",
	"BRUNEI DARUSSALAM" => "bn",
	"TIMOR-LESTE" => "tl",
	"SOLOMON ISLANDS" => "sb",
	"VANUATU" => "vu",
	"FIJI" => "fj",
	"PHILIPPINES" => "ph",
	"CHINA" => "cn",
	"TAIWAN" => "tw",
	"JAPAN" => "jp",
	"RUSSIA" => "ru",
	"UNITED STATES" => "us",
	"MAURITIUS" => "mu",
	"REUNION" => "re",
	"MADAGASCAR" => "mg",
	"COMOROS" => "km",
	"SEYCHELLES" => "sc",
	"MALDIVES" => "mv",
	"PORTUGAL" => "pt",
	"SPAIN" => "es",
	"CAPE VERDE" => "cv",
	"FRENCH POLYNESIA" => "pf",
	"SAINT KITTS AND NEVIS" => "kn",
	"ANTIGUA AND BARBUDA" => "ag",
	"DOMINICA" => "dm",
	"SAINT LUCIA" => "lc",
	"BARBADOS" => "bb",
	"GRENADA" => "gd",
	"TRINIDAD AND TOBAGO" => "tt",
	"DOMINICAN REPUBLIC" => "do",
	"HAITI" => "ht",
	"FALKLAND ISLANDS" => "fk",
	"ICELAND" => "is",
	"NORWAY" => "no",
	"SRI LANKA" => "lk",
	"CUBA" => "cu",
	"BAHAMAS" => "bs",
	"JAMAICA" => "jm",
	"ECUADOR" => "ec",
	"CANADA" => "ca",
	"GUATEMALA" => "gt",
	"HONDURAS" => "hn",
	"EL SALVADOR" => "sv",
	"NICARAGUA" => "ni",
	"COSTA RICA" => "cr",
	"PANAMA" => "pa",
	"COLOMBIA" => "co",
	"VENEZUELA" => "ve",
	"GUYANA" => "gy",
	"SURINAME" => "sr",
	"FRENCH GUIANA" => "gf",
	"PERU" => "pe",
	"BOLIVIA" => "bo",
	"PARAGUAY" => "py",
	"URUGUAY" => "uy",
	"ARGENTINA" => "ar",
	"CHILE" => "cl",
	"BRAZIL" => "br",
	"BELIZE" => "bz",
	"MONGOLIA" => "mn",
	"NORTH KOREA" => "kp",
	"SOUTH KOREA" => "kr",
	"KAZAKHSTAN" => "kz",
	"TURKMENISTAN" => "tm",
	"UZBEKISTAN" => "uz",
	"TAJIKISTAN" => "tj",
	"KYRGYZ REPUBLIC" => "kg",
	"AFGHANISTAN" => "af",
	"PAKISTAN" => "pk",
	"INDIA" => "in",
	"NEPAL" => "np",
	"BHUTAN" => "bt",
	"BANGLADESH" => "bd",
	"MYANMAR" => "mm",
	"THAILAND" => "th",
	"CAMBODIA" => "kh",
	"LAO PEOPLE'S DEMOCRATIC REPUBLIC" => "la",
	"VIETNAM" => "vn",
	"GEORGIA" => "ge",
	"ARMENIA" => "am",
	"AZERBAIJAN" => "az",
	"IRAN" => "ir",
	"TURKEY" => "tr",
	"OMAN" => "om",
	"UNITED ARAB EMIRATES" => "ae",
	"QATAR" => "qa",
	"KUWAIT" => "kw",
	"SAUDI ARABIA" => "sa",
	"SYRIAN ARAB REPUBLIC" => "sy",
	"IRAQ" => "iq",
	"JORDAN" => "jo",
	"LEBANON" => "lb",
	"ISRAEL" => "il",
	"CYPRUS" => "cy",
	"UNITED KINGDOM" => "gb",
	"IRELAND" => "ie",
	"SWEDEN" => "se",
	"FINLAND" => "fi",
	"LATVIA" => "lv",
	"LITHUANIA" => "lt",
	"BELARUS" => "by",
	"POLAND" => "pl",
	"ITALY" => "it",
	"FRANCE" => "fr",
	"NETHERLANDS" => "nl",
	"BELGIUM" => "be",
	"GERMANY" => "de",
	"DENMARK" => "dk",
	"SWITZERLAND" => "ch",
	"CZECH REPUBLIC" => "cz",
	"SLOVAKIA" => "sk",
	"AUSTRIA" => "at",
	"HUNGARY" => "hu",
	"SLOVENIA" => "si",
	"CROATIA" => "hr",
	"BOSNIA AND HERZEGOVINA" => "ba",
	"MALTA" => "mt",
	"UKRAINE" => "ua",
	"MOLDOVA" => "md",
	"ROMANIA" => "ro",
	"SERBIA" => "rs",
	"BULGARIA" => "bg",
	"ALBANIA" => "al",
	"MACEDONIA" => "mk",
	"GREECE" => "gr",
	"SINGAPORE" => "sg",
	"HONG KONG" => "hk"
);


// echo "<pre>";
// print_r($shortCodes);
// echo count($shortCodes);
// echo "</pre>";

// foreach ($shortCodes as $key => $value) {
// 	echo "\"".strtoupper($key)."\" => \"".$value."\",<br>";
// }


$cleanSheetData = array();
foreach ($sheetData as $key => $value) {
	if ($value["D"] == "X86" && !preg_match("/^~/", $value["C"]) && $value["I"] != NULL){
		$cleanSheetData[] = array("country" => $value["C"], "wai" => $value["I"], "shortCode" => $shortCodes[strtoupper($value["C"])]);
			unset($shortCodes[strtoupper($value["C"])]);
	}
}
// echo "========================<br>";
// echo "========================<br>";
// echo "========================<br>";
// echo "========================<br>";
// echo "========================<br>";

// echo "<pre>";
// print_r($shortCodes);
// echo count($shortCodes);
// echo "</pre>";
?>