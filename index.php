<?php require_once("excel_reader.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>IBM WAI visualiser</title>
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type">

	<link href="jqvmap/dist/jqvmap.css" media="screen" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="css/font-awesome-4.6.3/css/font-awesome.min.css">
	<style>
		html, body {
			padding: 0;
			margin: 0;
			width: 100%;
			height: 100%;
		}
		#vmap {
			width: 100%;
			height: 100%;
			background-color: red;
		}
	</style>

	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="jqvmap/dist/jquery.vmap.js"></script>
	<script type="text/javascript" src="jqvmap/dist/maps/jquery.vmap.world.js" charset="utf-8"></script>

	<script type="text/javascript">
		var wai = {
			<?php foreach ($cleanSheetData as $key => $value) {
				echo $value["shortCode"].": ".$value["wai"].",";
			} ?>

		}
		jQuery(document).ready(function () {
        // List of Regions we'll let clicks through for
        var enabledRegions = [
        <?php foreach ($cleanSheetData as $key => $value) {
        	echo "'".$value["shortCode"]."',";
        } ?>
        ];

        jQuery('#vmap').vectorMap({
        	map: 'world_en',
        	enableZoom: true,
        	showTooltip: true,
        	hoverColor: null,
        	colors: {
        		<?php foreach ($cleanSheetData as $key => $value) {
        			echo $value["shortCode"].": '";
        			if ($value["wai"] <= $firstBorder) {
        				echo "#".$settings->{'firstColor'}->{'value'};
        			} elseif ($value["wai"] <= $secondBorder) {
        				echo "#".$settings->{'secondColor'}->{'value'};
        			} else {
        				echo "#".$settings->{'thirdColor'}->{'value'};
        			}
        			echo "',";
        		} ?>
        	},

        	onResize: function (element, width, height) {
        		console.log('Map Size: ' +  width + 'x' +  height);
        	},


        	onRegionClick: function(event, code, region){
          // Check if this is an Enabled Region, and not the current selected on
          if(enabledRegions.indexOf(code) === -1 || currentRegion === code){
            // Not an Enabled Region
            event.preventDefault();
        } else {
            // Enabled Region. Update Newly Selected Region.
            currentRegion = code;
        }
    },
    onLabelShow: function(event, label, code){
    	if(enabledRegions.indexOf(code) === -1){
    		event.preventDefault();
    	}
    }



});
    });
</script>
<style>
	.src-selection {
		background-color: rgb(165, 191, 221);
		padding: 5px;
	}

	a {
		color: #000;
		padding: 5px;
	}

</style>
</head>

<body>	
	<div class="src-selection">
		<form action="./" method="post" style="display: inline;">
			<select name="srcId" onchange="this.form.submit()">
				<?php
				$i = 0;
				foreach($availableSources as $filename) {
					echo "<option value=\"";
					echo $i;
					echo "\"";
					if (isset($_POST['srcId']) && $i == $_POST['srcId']) {
						echo "selected=\"selected\"";
					}
					echo ">";
					echo basename($filename)." (uploaded at ".date("F d Y H:i:s.", filemtime($filename)).")";
					echo "</option>";
					$i++;
				} ?>
			</select>
		</form>
		<a href="settingsUpdate.php" target="_blank"><i class="fa fa-cog" style="float: right" aria-hidden="true"></i></a>
	</div>
	<div id="vmap"></div>

</body>
</html>