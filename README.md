# README #

This application stands for visualisation of WAI on the map.

### Requirements ###

* PHP version >= 5.4.40 (tested on this version)
* tested only with .xls files
* .xls files must be placed in "source_files" directory - this is the place, where script is looking for sources

### Some notes to .xls files structure ###

* Script reads only sheet, which is set in settingsUpdate.php
* Script takes only lines, where
	* in "D" column is value "X86"
	* in "I" column is a number (this cell cannot be empty)

Files in "source_files" are held there just for testing stuff. It's recommended to delete them and put there some files with real data.